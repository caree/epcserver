var timeFormater = require('./timeFormat').getCurrentTime;
var _                  = require('underscore');

var eventNewProducer = 'newProducer';
var eventDeleteProducer = 'deleteProducer';

function producer(_producerCode, _producerName, _note){
	this.producerCode = _producerCode;
	this.producerName = _producerName;
	this.note = _note;
	this.timeStamp = timeFormater();
}

exports.producerListIndex = function(req, res){
	res.render('producerListIndex');
};

exports.producerList = function(req, res){
	producerDBFind({}).then(function(_docs){
		var str = JSON.stringify(_docs);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log('error <= producerListIndex'.error);
		console.dir(error);
		res.send('error');
	})	
};
exports.producerListAll = function(_selector){
	if(_selector == null) _selector = {};
	return producerDBFind(_selector);
}
exports.addProducer = function(req, res){
	var body = req.body;
	var producerCode = body.producerCode;
	var producerName = body.producerName;
	var note = body.note;
	producerDBFind({$or: [{producerCode: producerCode}, {producerName: producerName}]}).then(function(_docs){
		if(_.size(_docs) > 0){
			throw new Error('duplicated');
		}
	}).then(function(){
		return producerDBInsert(new producer(producerCode, producerName, note));
	}).then(function(_newDoc){
		// _newDoc.state = 'ok';
		// var str = JSON.stringify(_newDoc);
		var str = "ok";
		console.log(str.data);
		g_EventProxy.emit('publishMsg', {event: eventNewProducer, msg: { sender: 'epcServer' }});
		res.send(str);
	}).catch(function(error){
		console.log('error <= addProducer'.error);
		console.dir(error);
		// console.log(error.message);
		res.send(error.message);
	})
};
exports.removeProducer = function(req, res){
	var producerCode = req.body.producerCode;
	producerDBRemove({producerCode: producerCode}).then(function(_number){
		if(_number > 0){
			// var state = {state: "ok"};
			// var str = JSON.stringify(state);
			g_EventProxy.emit('publishMsg', {event: eventDeleteProducer, msg: { sender: 'epcServer' }});
			var str = 'ok';
			console.log(str.data);
			res.send(str);
		}else{
			throw new Error('noData');
		}
	}).catch(function(error){
		console.log('error <= removeProducer'.error);
		console.dir(error);
		console.send(error.message);
	})
};










