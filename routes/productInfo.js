var timeFormater = require('./timeFormat').getCurrentTime;
var _                  = require('underscore');



function productInfo(_productEPC, _productCode){
	this.productEPC = _productEPC;
	this.productCode = _productCode;
	this.timeStamp = timeFormater();
}
productInfo.prototype.tryValidate = function(){
	if((this.productEPC == null || this.productEPC.length <=0 || 
		this.productCode == null || this.productCode.length <= 0)){
		return false;
	}else{
		return true;
	}
}

exports.productListIndex = function(req, res){
	productTypeInfoDBFind({}).then(function(_docs){
		// var docs = _.map(_docs, function(_doc){
		// 	return {productCode: _doc.productCode, productName: _doc.productName};
		// });
		res.render('productListIndex', {_productTypes: JSON.stringify(_docs)});
	}).catch(function(){
		console.log('get product type info list failed'.error);
		res.send('error');		
	})

};
exports.productlist = function(req, res){
	productInfodbFind({}).then(function(_docs){
		var str = JSON.stringify(_docs);
		console.log(str.data);
		res.send(str);
	}).catch(function(){
		console.log('error <= productlist'.error);
		res.send('error');
	})

};
// exports.resetEpcProductCodeDefault = function(_productCode){
// 	return productInfodbUpdate({productCode: _productCode}, {$set: {productCode: "xxxxxx"}}, { multi: true })
// 			.then(function(_numberReplaced){
// 				if(_numberReplaced > 0){
// 					g_EventProxy.emit('publishMsg', {event: eventProductCodeChanged, msg: { sender: 'epcServer' }});
// 				}
// 				return _numberReplaced;
// 			});
// }

exports.addProduct = function(req, res){
	var body = req.body;
	var productEPC = body.productEPC;
	var productCode = body.productCode;
	var productNew = new productInfo(productEPC, productCode);
	console.dir(productNew);
	

		
	if(productNew.tryValidate()){
		productInfodbFind({productEPC: productEPC})
		.then(function(_docs){
			if(_.size(_docs) > 0){
				throw new Error("duplicated");
			}
		}).then(function(){
			console.dir(productNew);
			return productInfodbInsert(productNew);
		}).then(function(_doc){
			// _doc.state = "ok";
			// var str = JSON.stringify(_doc);
			g_EventProxy.emit('publishMsg', {event: eventNewProduct, msg: { sender: 'epcServer' }});
			var str = "ok";
			console.log(str.data);
			res.send(str);
		}).catch(function(error){
			console.log('error <= addProduct'.error);
			console.dir(error);
			res.send(error.message);
		})
	}else{
		res.send('paraError')
	}



};

// exports.listEpc = function(req, res){
// 	productInfodbFind({}).then(function(_docs){
// 		var str = JSON.stringify(_docs);
// 		console.log(str.data);
// 		res.send(str);
// 	}).catch(function(error){
// 		console.log('error <= listEpc'.error);
// 		res.send('error');
// 	})


// 	// var ep = EventProxy.create('allEpc', function(_docs){
// 	// 	console.dir(_docs);
// 	// 	var docs = _.map(_docs, function(_doc){
// 	// 		return {epc: _doc.epc, itemType: _doc.itemType};
// 	// 	});
// 	// 	res.send(JSON.stringify(docs));
// 	// });
// 	// ep.fail(function(_err){
// 	// 	res.send('error');
// 	// });

// 	// productInfodb.find({}, ep.done('allEpc'));

// };
exports.removeAllEpc = function(req, res){
	// var ep = EventProxy.create('removeAllEpc', function(_numRemoved){
	// 	console.log(_numRemoved + ' EPC removed!');
	// 	res.send('ok');
	// });
	// ep.fail(function(_err){
	// 	console.dir(_err);
	// 	res.send('error');
	// });
	// //默认非多项删除
	// productInfodb.remove({}, {multi: true}, ep.done('removeAllEpc'));	
};

exports.addTestEpc = function(req, res){
	// var epclist = [(new productTypeInfo('909603F01234000001010203', '01')), 
	// 				(new productTypeInfo('AD94240019F7AD9151001004', '01')), 
	// 				(new productTypeInfo('AD94240019F7900000101002', '02'))];

	// var ep = new EventProxy();
	// ep.after('insertOK', _.size(epclist), function(_newDocs){
	// 	res.send('ok');
	// });
	// ep.fail(function(_err){
	// 	res.send('error');
	// });

	// _.each(epclist, function(_epc){
	// 	productInfodb.insert(_epc, ep.done('insertOK'));
	// });
};

exports.removeProduct = function(req, res){
	var productEPC = req.body.productEPC;

	productInfodbRemove({productEPC: productEPC}).then(function(_doc){
		// _doc.state = "ok";
		// var str = JSON.stringify(_doc);
		g_EventProxy.emit('publishMsg', {event: eventDeleteProduct, msg: { sender: 'epcServer' }});
		var str = 'ok';
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log('error <= removeEpc'.error);
		console.dir(error);
		res.send(error.message);
	})
};




