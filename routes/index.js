

var _                  = require('underscore');
var productInfo    = require('./productInfo');
var productTypeInfo    = require('./productTypeInfo');
var producer = require('./producer');
require('./communication');

// setInterval(function(){
// 	g_EventProxy.emit('publishMsg', {event: 'login', msg: { name: 'epcServer' }});
//   console.log('sending msg ...');
// }, 1000);


exports.indexAPI = function(req, res){
	res.render('indexAPI', {title:'API Test'});
}
exports.index = function(req, res){
	res.render('index', {title:'EPC公共信息管理系统', _system_name:'EPC公共信息管理系统', _company:'北京科技发展有限公司'});
}
exports.overview = function(req, res){
	res.render('overview');
};

exports.productTypeInfoExtend = function(req, res){
	productTypeInfo.listTypeInfoAll({}).then(function(_typeList){
		producer.producerListAll({}).then(function(_producerList){
			var list = _.map(_typeList, function(_type){
				var producer = _.findWhere(_producerList, {producerCode: _type.producerCode});
				if(producer == null) return null;
				_type.producerName = producer.producerName;
				return _type;
			});
			var str = JSON.stringify(_.without(list, null));
			console.log(str);
			res.send(str);
		})
	}).catch(function(error){
		console.log(('error <= addProducer ' + error.message).error);
		res.send(error.message);		
	})
}

//*********************************************************
