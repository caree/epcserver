
var timeFormater = require('./timeFormat').getCurrentTime;
var _                  = require('underscore');
var productInfo    = require('./productInfo');



function productTypeInfo(_productCode, _productName, _producerCode, _note){
	this.productCode = _productCode;
	this.productName = _productName;
	this.producerCode = _producerCode;
	this.timeStamp = timeFormater();
	this.note = _note;
	// this.collection = 'typeInfo';
}
productTypeInfo.prototype.validateProductTypeInfo = function(_callback){
	if((this.productCode != null) && (this.productName != null)){
		_callback(null, this);
	}else{
		_callback('validateProductTypeInfo error', this);
	}
}

exports.productTypelistIndex = function(req, res){
	producerDBFind({}).then(function(_docs){
		var str = JSON.stringify(_docs);
		console.log(str.data);
		res.render('productTypelistIndex', {producers: str});
	}).catch(function(error){
		console.log('error <= productTypelistIndex'.error);
		console.dir(error);
		res.render('errorPage');
	})		
};
exports.addTypeInfo = function(req, res){
	var body = req.body;
	var productCode = body.productCode;
	var productName = body.productName;
	var producerCode = body.producerCode;
	var note = body.note;

	// db.find({ $or: [{ planet: 'Earth' }, { planet: 'Mars' }] }, function (err, docs) {
	//   // docs contains Earth and Mars
	// });
	productTypeInfoDBFind({$or: [{productCode: productCode}, {productName: productName}]})
	.then(function(_docs){
		if(_.size(_docs) > 0){
			throw new Error("duplicated");
		}
	}).then(function(){
		var productTypeInfoNew = new productTypeInfo(productCode, productName, producerCode, note);
		return productTypeInfoDBInsert(productTypeInfoNew);
	}).then(function(_doc){
		// _doc.state = "ok";
		g_EventProxy.emit('publishMsg', {event: eventNewTypeInfo, msg: { sender: 'epcServer' }});
		var str = 'ok';
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log('error <= addTypeInfo'.error);
		console.dir(error);
		res.send(error.message);
	})

};
exports.listTypeInfo = function(req, res){
	productTypeInfoDBFind({}).then(function(_docs){
		var str = JSON.stringify(_docs);
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log('get type info list failed'.error);
		console.dir(error);
		res.send(error.message);
	})

};
exports.listTypeInfoAll = function(_selector){
	if(_selector == null) _selector = {};
	return productTypeInfoDBFind(_selector);
}
exports.addTestTypeInfo = function(req, res){

}
exports.removeTypeInfo = function(req, res){
	var productCode = req.body.productCode;

	// console.log(productCode);
	productTypeInfoDBRemove({productCode: productCode}).then(function(_number){
		if(_number > 0){
			g_EventProxy.emit('publishMsg', {event: eventDeleteTypeInfo, msg: { sender: 'epcServer' }});
		}
		return productInfodbRemove({productCode: productCode});
	}).then(function(_number){
		if(_number > 0){
			g_EventProxy.emit('publishMsg', {event: eventDeleteProduct, msg: { sender: 'epcServer' }});
		}
		var str = "ok";
		console.log(str.data);
		res.send(str);
	}).catch(function(error){
		console.log('error <= removeTypeInfo'.error);
		console.dir(error);
		res.send(error.message);
	})

};
