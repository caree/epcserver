
/**
 * Module dependencies.
 */
require('./config');


eventNewTypeInfo = 'epcServer:newTypeInfo';
eventDeleteTypeInfo = 'epcServer:deleteTypeInfo';
eventNewProduct = 'epcServer:newProduct';
eventDeleteProduct = 'epcServer:deleteProduct';


var colors = require('colors');

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});
var EventProxy         = require('eventproxy');
g_EventProxy = new EventProxy();

var Q                  = require('q');
var Datastore          = require('nedb');
var productInfodb      = new Datastore({ filename: 'epc.db', autoload: true });
var productTypeInfoDB  = new Datastore({filename: 'typeInfo.db', autoload: true});
var producerDB         = new Datastore({filename: 'producer.db', autoload: true});


productInfodbFind = Q.nbind(productInfodb.find, productInfodb);
productInfodbRemove = Q.nbind(productInfodb.remove, productInfodb);
productInfodbInsert = Q.nbind(productInfodb.insert, productInfodb);
productInfodbUpdate = Q.nbind(productInfodb.update, productInfodb);


productTypeInfoDBFind = Q.nbind(productTypeInfoDB.find, productTypeInfoDB);
productTypeInfoDBRemove = Q.nbind(productTypeInfoDB.remove, productTypeInfoDB);
productTypeInfoDBInsert = Q.nbind(productTypeInfoDB.insert, productTypeInfoDB);

producerDBFind = Q.nbind(producerDB.find, producerDB);
producerDBRemove = Q.nbind(producerDB.remove, producerDB);
producerDBInsert = Q.nbind(producerDB.insert, producerDB);



var request = require('request');
httpRequestGet = Q.denodeify(request.get);
httpRequestPost = Q.denodeify(request.post);


var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var productInfo    = require('./routes/productInfo');
var productTypeInfo    = require('./routes/productTypeInfo');
var producer    = require('./routes/producer');

var app = express();

// all environments
app.set('port', process.env.PORT || 6010);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
// app.use(function(req, res, next) {
//     var data = '';
//     req.setEncoding('utf8');
//     // console.dir(req.headers);
//     req.on('data', function(chunk) {
//         // console.log(chunk); 
//         data += chunk;
//     });
//     req.on('end', function() {
//         req.rawBody = data;
//         // console.log("next => ");
//         console.log(data);
//         next();
//     });
// });
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
app.get('/', routes.index);
app.get('/indexapi', routes.indexAPI);
app.get('/overview', routes.overview);
app.post('/productTypeInfoExtend', routes.productTypeInfoExtend);

app.get('/productListIndex', productInfo.productListIndex);
app.post('/productlist', productInfo.productlist);
app.post('/addProduct', productInfo.addProduct);
// app.get('/listEpc', productInfo.listEpc);
app.get('/addTestEpc', productInfo.addTestEpc);
app.get('/removeAllEpc', productInfo.removeAllEpc);
app.post('/removeProduct', productInfo.removeProduct);


app.get('/productTypelistIndex', productTypeInfo.productTypelistIndex);
app.post('/addTypeInfo', productTypeInfo.addTypeInfo);
app.post('/listTypeInfo', productTypeInfo.listTypeInfo);
app.get('/addTestTypeInfo', productTypeInfo.addTestTypeInfo);
// app.get('/removeAllTypeInfo', productTypeInfo.removeAllTypeInfo);
app.post('/removeTypeInfo', productTypeInfo.removeTypeInfo);


app.get('/producerListIndex', producer.producerListIndex);
app.post('/producerList', producer.producerList);
app.post('/addProducer', producer.addProducer);
app.post('/removeProducer', producer.removeProducer);


http.createServer(app).listen(app.get('port'), function(){
  console.log('EPC server listening on port ' + app.get('port'));
});
